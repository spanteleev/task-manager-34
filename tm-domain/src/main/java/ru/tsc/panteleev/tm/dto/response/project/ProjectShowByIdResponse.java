package ru.tsc.panteleev.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;

@NoArgsConstructor
public class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable Project project) {
        super(project);
    }

}
