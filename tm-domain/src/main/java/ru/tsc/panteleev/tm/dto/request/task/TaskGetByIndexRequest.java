package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskGetByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token);
        this.index = index;
    }

}
