package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.service.IProjectService;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable String name, @Nullable String description,
                          @Nullable Date dateBegin, @Nullable Date dateEnd
    ) {
        final Project project = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId, @Nullable Integer index, @Nullable String name,
                                 @Nullable String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Project project = findByIndex(userId, index);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable String id, @Nullable String name,
                              @Nullable String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findById(userId, id);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable Integer index,
                                       @Nullable Status status
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Project project = findByIndex(userId, index);
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusById(@Nullable final String userId, @Nullable String id,
                                    @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = findById(userId, id);
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

}
