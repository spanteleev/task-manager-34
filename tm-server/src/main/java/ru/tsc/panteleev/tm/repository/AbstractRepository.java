package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new Vector<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findById(@NotNull final String id) {
        return models.stream()
                .filter(model -> id.equals(model.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @Nullable
    @Override
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findById(id);
        return model == null ? null : remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findByIndex(index);
        return model == null ? null : remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Override
    public long getSize() {
        return models.size();
    }

}
